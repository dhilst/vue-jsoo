include Ezjs_min

type top = Unsafe.top
type any = Unsafe.any

let def_list f = function
  | [] -> undefined
  | l -> def (f l)

let manip_list f a = of_list @@ f @@ to_list a

type 'a table = 'a Table.t

type 'a table_cons = T of 'a table | L of (string * 'a) list

let to_table : 'a table_cons -> 'a table = function
  | T t -> t
  | L l -> Table.make l

let to_tablef f : 'a table_cons -> 'a table = function
  | T t -> t
  | L l -> Table.makef f l

let to_table_def : 'a table_cons -> 'a table optdef = function
  | T t -> if Table.length t = 0 then undefined else def t
  | L l -> def_list Table.make l

let to_tablef_def f : 'a table_cons -> 'a table optdef = function
  | T t -> if Table.length t = 0 then undefined else def t
  | L l -> def_list (Table.makef f) l

let to_any = Unsafe.inject
let coerce = Unsafe.coerce

let to_any_table f = function
  | L l -> L (List.map (fun (k, v) -> k, f v) l)
  | T t -> T (Table.(make (itemsf f t)))

class type unit_promise = [unit, error t] Promise.promise0
